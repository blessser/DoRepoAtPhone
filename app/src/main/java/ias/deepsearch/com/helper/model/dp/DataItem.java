package ias.deepsearch.com.helper.model.dp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vector on 16/7/6.
 */
public class DataItem {
    @SerializedName("value")@Expose
    String value;
    @SerializedName("class")@Expose
    String className;
    @SerializedName("key")@Expose
    String key;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
