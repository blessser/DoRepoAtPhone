package ias.deepsearch.com.helper.model.dp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vector on 16/8/11.
 */
public class Action {
    @SerializedName("type")@Expose
    String type;
    @SerializedName("node")@Expose
    ActionNode actionNode;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ActionNode getActionNode() {
        return actionNode;
    }

    public void setActionNode(ActionNode actionNode) {
        this.actionNode = actionNode;
    }

    public class ActionNode{
        @SerializedName("xpath")@Expose
        String xpath;
        @SerializedName("resource_id")@Expose
        String resource_id;

        public String getXpath() {
            return xpath;
        }

        public void setXpath(String xpath) {
            this.xpath = xpath;
        }

        public String getResource_id() {
            return resource_id;
        }

        public void setResource_id(String resource_id) {
            this.resource_id = resource_id;
        }
    }
}
