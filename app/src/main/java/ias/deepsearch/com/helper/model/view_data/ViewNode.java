package ias.deepsearch.com.helper.model.view_data;

import android.view.View;

import com.alibaba.fastjson.annotation.JSONField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ias.deepsearch.com.helper.util.normal.SerializeUtil;
import ias.deepsearch.com.helper.util.normal.ViewUtil;

/**
 * Created by vector on 16/5/11.
 */
public class ViewNode implements Serializable, Comparable{

    //表示View的类型，例如TextView等（类名）
    private String viewTag;
    private int total_view;

    public String getViewText() {
        return viewText;
    }

    public void setViewText(String viewText) {
        this.viewText = viewText;
    }

    private String viewText;

    private String resource_id;

    private String text;

    @JSONField(serialize=false)
    private transient View view;

    private int nodeHash;


    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    //在树种的层级
    private int depth;

    //view的子节点
    private List<ViewNode> children;

    //view的父节点
    @JSONField(serialize=false)
    private ViewNode parent;

    private int width;
    private int height;
    private int x;
    private int y;


    private String id;


    private int nodeRelateHash;

    private int viewNodeIDRelative;


    boolean isList;


    public int getNodeHash() {
        return nodeHash;
    }

    public void setNodeHash(int nodeHash) {
        this.nodeHash = nodeHash;
    }

    public int getNodeRelateHash() {
        return nodeRelateHash;
    }

    public void setNodeRelateHash(int nodeRelateHash) {
        this.nodeRelateHash = nodeRelateHash;
    }

    public void setList(boolean list) {
        isList = list;
    }

    public boolean isList() {
        return isList;
    }

    public int getViewNodeIDRelative() {
        return viewNodeIDRelative;
    }

    public void setViewNodeIDRelative(int viewNodeIDRelative) {
        this.viewNodeIDRelative = viewNodeIDRelative;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public String getViewTag() {
        return viewTag;
    }

    public void setViewTag(String viewTag) {
        this.viewTag = viewTag;
    }


    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }


    public List<ViewNode> getChildren() {
        return children;
    }

    public void setChildren(List<ViewNode> children) {
        this.children = children;
    }

    public ViewNode getParent() {
        return parent;
    }

    public void setParent(ViewNode parent) {
        this.parent = parent;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getTotal_view() {
        return total_view;
    }

    public void setTotal_view(int total_view) {
        this.total_view = total_view;
    }

    public String calStringWithoutPosition(){
        return  SerializeUtil.getAbbr(this.viewTag) + "-" + this.depth;
    }


    public ViewNode() {
    }
    public ViewNode findRootNode(){
        ViewNode root = parent;
        while(root != null){
           if(root.parent != null)
               root = root.parent;
            else
               break;
        }
        return root;
    }


    public String getXpath(){
        ViewNode vn = this;
        List<String> list = new ArrayList<>();
        while (vn != null){
            list.add(ViewUtil.getLast(vn.getViewTag()));
            vn = vn.getParent();
        }
        String res = "";
        int len = list.size();
        if (len > 0){
            res = list.get(len-1);
            if (len > 1){
                for (int i = len-2; i >= 0; --i) {
                    res += ("/" + list.get(i));
                }
            }
        }
        return res;
    }

    public String getResource_id() {
        return resource_id;
    }

    public void setResource_id(String resource_id) {
        this.resource_id = resource_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    //返回的是class+深度+位置
    public String calString(){
        String res = SerializeUtil.getAbbr(this.viewTag) + "-" + depth + "-" + viewText;
        return res + "-" + (x + width/2) + "-" + (y + height/2);
//        if (XY != null) {
//            return res + "-" + XY.getX() + "-" + XY.getY();
//        }
//        return res;
    }

    @Override
    public int compareTo(Object another) {
        int res = getY() - ((ViewNode) another).getY();
        if (res != 0)
            return res;
        res = getNodeRelateHash() - ((ViewNode) another).getNodeRelateHash();
        if (res != 0)
            return res;
        return getX() - ((ViewNode) another).getX();
    }
}
