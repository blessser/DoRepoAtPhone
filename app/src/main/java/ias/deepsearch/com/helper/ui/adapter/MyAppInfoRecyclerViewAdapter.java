package ias.deepsearch.com.helper.ui.adapter;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import ias.deepsearch.com.helper.R;
import ias.deepsearch.com.helper.model.dp.AppInfo;
import ias.deepsearch.com.helper.util.net.ApiInterface;

import java.util.List;


public class MyAppInfoRecyclerViewAdapter extends RecyclerView.Adapter<MyAppInfoRecyclerViewAdapter.ViewHolder> {

    private List<AppInfo> mValues;
    private final OnItemCheckedListener mListener;

    PackageManager pm ;

    public MyAppInfoRecyclerViewAdapter(Context context, List items, OnItemCheckedListener listener) {
        mValues = items;
        mListener = listener;
        pm = context.getPackageManager();
    }

    public void update(List items){
        mValues = items;
        notifyDataSetChanged();
    }


    public void checked(int i, boolean checked){
        mValues.get(i).hasKnowledge = checked;
        notifyItemChanged(i);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_appinfo, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        holder.tv_app_name.setText(holder.mItem.appName + " " + holder.mItem.versionName+"_" + holder.mItem.versionCode);
        holder.imv_app_logo.setImageURI(Uri.parse(ApiInterface.HOST + "appicons/" + holder.mItem.packageName + ".png"));
        holder.cb_knowledge.setChecked(holder.mItem.hasKnowledge);

        holder.cb_knowledge.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    mListener.onItemChecked(position, holder.mItem);
                }
            }
        });


        holder.container_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(position, holder.mItem);
            }
        });


    }

    @Override
    public int getItemCount() {
        return mValues == null ? 0: mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tv_app_name;
        public final SimpleDraweeView imv_app_logo;
        public final CheckBox cb_knowledge;
        public final LinearLayout container_ll;
        public AppInfo mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            tv_app_name = (TextView) view.findViewById(R.id.tv_app_name);
            container_ll = (LinearLayout) view.findViewById(R.id.container_ll);
            imv_app_logo = (SimpleDraweeView) view.findViewById(R.id.imv_app_logo);
            cb_knowledge = (CheckBox) view.findViewById(R.id.cb_knowledge);
        }
    }

    public interface OnItemCheckedListener{
        public void onItemChecked(int i, AppInfo item);
        public void onItemChanged(int i, boolean checked);
        public void onItemClick(int i, AppInfo appInfo);
    }
}
