package ias.deepsearch.com.helper.model.util;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UtilViewNode {
    @SerializedName("className")@Expose
    String className;
    @SerializedName("xpath")@Expose
    String xpath;
    @SerializedName("localtion")@Expose
    int[] location;
    @SerializedName("width")@Expose
    int width;
    @SerializedName("height")@Expose
    int height;

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

    public int[] getLocation() {
        return location;
    }

    public void setLocation(int[] location) {
        this.location = location;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public UtilViewNode() {
    }

    public UtilViewNode(String className, String xpath, int[] location, int width, int height) {
        this.className = className;
        this.xpath = xpath;
        this.location = location;
        this.width = width;
        this.height = height;
    }
}
