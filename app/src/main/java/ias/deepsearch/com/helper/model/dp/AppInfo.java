package ias.deepsearch.com.helper.model.dp;

/**
 * Created by vector on 16/7/5.
 */
public class AppInfo {
    public String appName;
    public String packageName;
    public String versionName;
    public String knowledgeUpdateTime;
    public int versionCode;
    public int port;
    public boolean hasKnowledge;
    public String md5;
}
