package ias.deepsearch.com.helper.model.dp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by vector on 16/7/6.
 */
public class IntentInfo {
    @SerializedName("datas")@Expose
    List<DataItem> datas;
    @SerializedName("sourceClass")@Expose
    String sourceClass;
    @SerializedName("intentUri")@Expose
    String intentUri;
    @SerializedName("deepLink")@Expose
    String deepLink;



    public List<DataItem> getDatas() {
        return datas;
    }

    public void setDatas(List<DataItem> datas) {
        this.datas = datas;
    }

    public String getSourceClass() {
        return sourceClass;
    }

    public void setSourceClass(String sourceClass) {
        this.sourceClass = sourceClass;
    }

    public String getIntentUri() {
        return intentUri;
    }

    public void setIntentUri(String intentUri) {
        this.intentUri = intentUri;
    }

    public String getDeepLink() {
        return deepLink;
    }

    public void setDeepLink(String deepLink) {
        this.deepLink = deepLink;
    }
}
