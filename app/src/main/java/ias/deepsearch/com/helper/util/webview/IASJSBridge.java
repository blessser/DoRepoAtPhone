package ias.deepsearch.com.helper.util.webview;

import android.webkit.JavascriptInterface;

import com.socks.library.KLog;

import ias.deepsearch.com.helper.util.normal.Constants;

/**
 * Created by vector on 16/8/9.
 */
public class IASJSBridge {


    @JavascriptInterface
    public void setWebContent(String content){
        KLog.v(Constants.WEB_TAG, "************message from webview*******");
        KLog.v(Constants.WEB_TAG, content);
    }
}
