package ias.deepsearch.com.helper.xposed.module;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;

import com.socks.library.KLog;

import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import ias.deepsearch.com.helper.receive.ActivityReceiver;
import ias.deepsearch.com.helper.util.BuildConfig;

/**
 * Created by vector on 16/8/4.
 * 只用来配合工具，主要是用来查看页面结构和打印intent序列
 */
public class ActivityOnCreateHook extends XC_MethodHook {

    XC_LoadPackage.LoadPackageParam loadPackageParam;
    
    public ActivityOnCreateHook(XC_LoadPackage.LoadPackageParam loadPackageParam) {
        this.loadPackageParam = loadPackageParam;
    }

    @Override
    protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
        super.beforeHookedMethod(param);
    }
    @Override
    protected void afterHookedMethod(MethodHookParam param) throws Throwable {
        super.afterHookedMethod(param);

        final Context context = (Context) param.thisObject;

        Activity activity = (Activity) param.thisObject;
        String activityName = activity.getClass().getName();

        KLog.v("liuyi","=======onCreate========: " + activityName);


        Intent intent = activity.getIntent();
        Uri data = intent.getData();
        if(data != null){
            KLog.v("deeplink",activityName);
            KLog.v("deeplink", data.toString());
        }

        KLog.v(BuildConfig.GETVIEW, "#*#*#*#*#*#*# enable receiver in: " + activityName);
        injectReceiver(context, activity);
    }

    private void injectReceiver(Context context, Activity activity) {
        //注册一个广播接收器，可以用来接收指令，这里是用来回去指定view的xpath路径的
        BroadcastReceiver receiver = new ActivityReceiver(activity);
        IntentFilter filter = new IntentFilter(ActivityReceiver.myAction);
        filter.addAction(ActivityReceiver.findView);
        filter.addAction(ActivityReceiver.getIntent);
        filter.addAction(ActivityReceiver.getViewTree);

        XposedHelpers.setAdditionalInstanceField(activity, "iasReceiver", receiver);

        context.registerReceiver(receiver, filter);
    }
}
