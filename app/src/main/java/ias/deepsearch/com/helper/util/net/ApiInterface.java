package ias.deepsearch.com.helper.util.net;

import com.squareup.okhttp.ResponseBody;

import retrofit.http.GET;
import retrofit.http.Path;
import rx.Observable;

/**
 * Created by vector on 17/2/23.
 */

public interface ApiInterface {

    String HOST = "http://139.129.208.65:8080/";
    @GET("http://139.129.208.63:8080/apps/dexs/{name}")
    Observable<ResponseBody> loadDex(@Path("name") String name);
}