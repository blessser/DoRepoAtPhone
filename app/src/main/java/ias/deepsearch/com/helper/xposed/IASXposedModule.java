package ias.deepsearch.com.helper.xposed;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.XC_MethodHook;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.callbacks.XC_LoadPackage;
import ias.deepsearch.com.helper.receive.ActivityReceiver;
import ias.deepsearch.com.helper.xposed.module.ActivityOnCreateHook;
import ias.deepsearch.com.helper.xposed.module.WebViewHook;


public class IASXposedModule implements IXposedHookLoadPackage{


    public static int now = 0;
    public static String launchActivity = "";
    public static String targetActivity = "";
    public static String ser = "";

    @Override
    public void handleLoadPackage(final XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {

        XposedHelpers.findAndHookMethod("android.app.Activity", loadPackageParam.classLoader, "onCreate", Bundle.class, new ActivityOnCreateHook(loadPackageParam));

        //开启WebView的远程debug
        XposedBridge.hookAllConstructors(WebView.class, new WebViewHook(loadPackageParam));


        //我们在onCreate中给应用注册了一些广播接收器，然后在onDestroy中需要撤销这些广播接收器
        XposedHelpers.findAndHookMethod("android.app.Activity", loadPackageParam.classLoader, "onDestroy", new XC_MethodHook() {
            @Override
            protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
                super.beforeHookedMethod(param);
            }

            @Override
            protected void afterHookedMethod(MethodHookParam param) throws Throwable {
                super.afterHookedMethod(param);
                ActivityReceiver receiver = (ActivityReceiver) XposedHelpers.getAdditionalInstanceField(param.thisObject,"iasReceiver");
                if(receiver != null) {
                    ((Activity) param.thisObject).unregisterReceiver(receiver);
                    XposedHelpers.setAdditionalInstanceField(param.thisObject, "iasReceiver", null);
                }
            }
        });
    }







}
