package ias.deepsearch.com.helper.util.normal;

import java.io.InputStream;
import java.security.MessageDigest;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class MD5Calculator {
    public static String calMD5(String zipPath) {
        try {
            ZipFile zf = new ZipFile(zipPath);
            ZipEntry ze = zf.getEntry("META-INF/MANIFEST.MF");
            InputStream in = zf.getInputStream(ze);
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            int numRead;
            byte[] buffer = new byte[4096];
            while ((numRead = in.read(buffer)) > 0) {
                md5.update(buffer, 0, numRead);
            }
            zf.close();
            return toHexString(md5.digest());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "0000000000000000";
    }

    private static final char HEX_DIGITS[] = { '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    public static String toHexString(byte[] b) {
        StringBuilder sb = new StringBuilder(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);
            sb.append(HEX_DIGITS[b[i] & 0x0f]);
        }
        return sb.toString();
    }
}