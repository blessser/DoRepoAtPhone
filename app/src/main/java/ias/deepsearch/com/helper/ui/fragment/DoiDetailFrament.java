package ias.deepsearch.com.helper.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.yancloud.android.contractclient.databaseHelper.DatabaseDao;
import com.yancloud.android.contractclient.databaseHelper.Do;
import com.yancloud.android.contractclient.doip.HandleService;
import com.yancloud.android.manager.IAPIBinder;
import com.yancloud.android.manager.IAppConnector;
import com.yancloud.android.reflection.get.CMDManager;

import java.util.ArrayList;
import java.util.List;

import ias.deepsearch.com.helper.R;
import ias.deepsearch.com.helper.ui.activity.HomeActivity;

public class DoiDetailFrament extends Fragment {

    private static DatabaseDao databaseDao = DatabaseDao.getInstance();
    private String info = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.doi_show, container, false);
        TextView doiName = (TextView) view.findViewById(R.id.doi_name);
        TextView packageName = (TextView)view.findViewById(R.id.packageNmae);
        TextView methodName = (TextView)view.findViewById(R.id.methodName);
        TextView callNumber = (TextView)view.findViewById(R.id.callNumber);
        TextView desp = (TextView)view.findViewById(R.id.methodDesp);

        final TextView invokeResult = (TextView)view.findViewById(R.id.result);


        //获取Bundle的信息
         info=getArguments().getString("doi");
//        final Do doObject= databaseDao.getInfoByDoi(info);
//        doiName.setText(info);
//        packageName.setText(doObject.getPackageName());
//        methodName.setText(doObject.getMethodName());
//        callNumber.setText(String.valueOf(doObject.getCallNumber()));
//        desp.setText(doObject.getDesp());
//        Log.d("test", String.valueOf(doObject.getCallNumber()));
//        Button invoke = view.findViewById(R.id.invoke);
//        invoke.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//               HomeActivity context = (HomeActivity) getActivity();
//            try{
//               IBinder iBinder = context.conn.request(doObject.getPackageName(), "1");
//                IAPIBinder app = IAPIBinder.Stub.asInterface(iBinder);
//                    Log.d("点击调用","     包名："+doObject.getPackageName()+"方法名"+doObject.getMethodName());
//                    String result = app.callMethod(doObject.getMethodName(),"CommPoter");
//                    invokeResult.setText(result);
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                }
//
//
//
//
//
//            }
//        });


        return view;
    }

}
