package ias.deepsearch.com.helper.ui.fragment;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.drawee.view.SimpleDraweeView;
import com.google.gson.Gson;

import ias.deepsearch.com.helper.R;
import ias.deepsearch.com.helper.model.dp.AppInfo;
import ias.deepsearch.com.helper.util.ShellUtils;
import ias.deepsearch.com.helper.util.net.ApiInterface;

/**
 * Created by vector on 17/2/25.
 */

public class AppManagementFragment extends DialogFragment{


    AppInfo appInfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            String info = getArguments().getString("appInfo");
            appInfo = new Gson().fromJson(info, AppInfo.class);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder((getActivity()));
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.fragment_app_management, null);

        initView(view);

//        builder.setView(view)
//                .setNegativeButton("关闭",null);
        builder.setView(view);
        return builder.create();
    }

    void initView(View view){
        TextView tv_app_name = (TextView) view.findViewById(R.id.tv_app_name);
        TextView tv_app_packagename = (TextView) view.findViewById(R.id.tv_app_packagename);
        SimpleDraweeView imv_app_logo = (SimpleDraweeView) view.findViewById(R.id.imv_app_logo);
        TextView tv_md5 = (TextView) view.findViewById(R.id.tv_md5);
        TextView tv_end_app = (TextView) view.findViewById(R.id.tv_end_app);
        TextView tv_update_time = (TextView) view.findViewById(R.id.tv_update_time);

        tv_app_name.setText(appInfo.appName + " " + appInfo.versionName+"_" + appInfo.versionCode);
        tv_app_packagename.setText(appInfo.packageName);
        tv_update_time.setText(appInfo.knowledgeUpdateTime);
        tv_md5.setText(appInfo.md5);
        imv_app_logo.setImageURI(Uri.parse(ApiInterface.HOST + "appicons/" + appInfo.packageName + ".png"));


        tv_end_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getActivity(), "正在杀死 " + appInfo.appName, Toast.LENGTH_SHORT).show();
                ShellUtils.execCommand("sh /sdcard/script/allkill.sh " + appInfo.packageName,true);
            }
        });


    }
}
