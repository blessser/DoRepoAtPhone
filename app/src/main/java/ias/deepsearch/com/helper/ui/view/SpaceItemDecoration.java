package ias.deepsearch.com.helper.ui.view;

/**
 * Created by vector on 17/2/23.
 */

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by vector on 16/1/4.
 * 用来处理RecyclerView的间距（android 5.0以前默认是有的，5.0以后没有）
 */
public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int space;

    public SpaceItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        if (parent.getChildPosition(view) != 0)
            outRect.top = space;
    }
}