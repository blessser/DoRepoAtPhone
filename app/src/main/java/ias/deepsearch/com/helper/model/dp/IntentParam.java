package ias.deepsearch.com.helper.model.dp;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by vector on 16/8/11.
 * [
     {
     "index": 0,
     "name": "keyword",
     "type": "String"
     }
   ]
 */
public class IntentParam {
    @SerializedName("index")@Expose
    int index;
    @SerializedName("name")@Expose
    String name;
    @SerializedName("type")@Expose
    String type;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
