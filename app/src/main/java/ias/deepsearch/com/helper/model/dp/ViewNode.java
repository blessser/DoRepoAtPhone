package ias.deepsearch.com.helper.model.dp;

import android.view.View;

/**
 * Created by vector on 16/8/5.
 */
public class ViewNode {
    View view;
    ViewNode parent;

    public ViewNode(View view, ViewNode parent) {
        this.view = view;
        this.parent = parent;
    }

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public ViewNode getParent() {
        return parent;
    }

    public void setParent(ViewNode parent) {
        this.parent = parent;
    }
}
