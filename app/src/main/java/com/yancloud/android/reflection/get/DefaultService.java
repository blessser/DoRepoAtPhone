package com.yancloud.android.reflection.get;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by root on 8/12/17.
 */

public class DefaultService extends Service {
    AppConnectorImpl conn;
    public DefaultService(Context c) {
        conn = new AppConnectorImpl();
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return conn;
    }
}
