package com.yancloud.android.reflection.get;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by root on 6/29/17.
 */

public class Task implements Parcelable {
    public String pkgName;
    public String method;
    public String arg;
    public Task(Parcel in) {
        pkgName = in.readString();
        method = in.readString();
        arg = in.readString();
    }
    public Task() {

    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };



    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pkgName);
        dest.writeString(method);
        dest.writeString(arg);
    }
}
