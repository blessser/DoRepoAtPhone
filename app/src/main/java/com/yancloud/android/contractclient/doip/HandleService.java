package com.yancloud.android.contractclient.doip;

import android.util.Log;

import com.google.gson.Gson;

import bdware.doip.codec.DoHandleRecord;
import bdware.doip.codec.HRRegister;
import bdware.doip.codec.ServiceHandleRecord;
import bdware.doip.codec.bean.DOIPServiceInfo;

public class HandleService{

    public static HRRegister hr = HRRegister.getStaticRegister();
    private static String repoID = "86.5000.470/doip.android123";
    private static String ownerID = "86.5000.470/dou.SUPER";

    public HandleService(HRRegister hr){
        this.hr = hr;
    }

    //todo
    // register DOI in handle system
    public static String RegisterAPI(String app, String api, String description){
        DoHandleRecord dhr = new DoHandleRecord();
        dhr.identifier = null;
//        dhr.desc = "APP: "+ app + ",API: " + api+ ", Description: " + description;
        dhr.owner = ownerID;
        dhr.repository = repoID;
        dhr.pubkey = "";
        String resp = hr.register(dhr);
        System.out.println(resp);
        HSResponse HRresp = new Gson().fromJson(resp,HSResponse.class);
        Log.d("DOIP","message from handle service"+ resp);

        return HRresp.identifier;
    }

    // Update registry in handle system
    public static String UpdateAPI(String doi,String app, String api, String description){
        DoHandleRecord dhr = new DoHandleRecord();
        dhr.identifier = doi;
//        dhr.desc = "APP: "+ app + ",API: " + api+ ", Description: " + description;
        dhr.owner = ownerID;
        dhr.repository = repoID;
        String resp = hr.reregister(dhr);
        HSResponse HRresp = new Gson().fromJson(resp,HSResponse.class);
//        String resp = hr.register(new DoHandleRecord());
//        Log.d("DOIP","message from handle service"+ resp);
        return HRresp.status;
    }

    // register device in handle system
    public static String RegisterPhone(String IMEI, String description){
        return getRepoID();
    }

    public static String RegisterRepo(DOIPServiceInfo s){
        return hr.reregister(s.toServiceHandleRecord());
    }


    public static String getRepoID(){
        return repoID;
    }

    public static String getOwnerID(){

        return ownerID;
    }



    public class HSResponse{
        String identifier;
        String response;
        String status;
    }
}
