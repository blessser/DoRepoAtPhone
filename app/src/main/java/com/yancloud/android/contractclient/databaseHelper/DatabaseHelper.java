package com.yancloud.android.contractclient.databaseHelper;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

import ias.deepsearch.com.helper.ui.activity.HomeActivity;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "doi_db";//数据库名字
    private static final int DATABASE_VERSION = 1;//数据库版本号
    private static final String CREATE_TABLE = "create table doiTable ("
         //   + "id integer primary key autoincrement,"
            + "doi text primary key,"
            + "package text, "  //
            + "method text, "
            + "desp text, "
            +"isopen integer,"
            + "callNumber integer)"; //数据库里的表
    /*public DatabaseHelper getDatabaseInstance(){
        return new DatabaseHelper();
    }*/
    private  static DatabaseHelper databaseHelper;
    public static DatabaseHelper getInstance(Context context){
        if (databaseHelper==null) {
             databaseHelper = new DatabaseHelper(context, "doi_db", null, 1);
        }
        return databaseHelper;
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factoryr, int version) {
        super(context, "doi_db", null, 1);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
