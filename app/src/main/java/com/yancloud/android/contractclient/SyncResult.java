package com.yancloud.android.contractclient;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import io.netty.util.HashedWheelTimer;
import io.netty.util.Timeout;
import io.netty.util.TimerTask;

public class SyncResult {
	Map<String, ResultCallback> waitObj = new HashMap<String, ResultCallback>();
	static Gson gson = new Gson();
	final public static HashedWheelTimer timer = new HashedWheelTimer(Executors.defaultThreadFactory(), 5,
			TimeUnit.MILLISECONDS, 2);

	public void wakeUp(String requestID, String result) {
		ResultCallback ob = waitObj.get(requestID);
		waitObj.remove(requestID);
		if (ob != null)
			ob.onResult(result);
	}

	public void sleep(final String requestID, ResultCallback cb) {
		// TODO 访问控制Here
		if (!waitObj.containsKey(requestID)) {
			waitObj.put(requestID, cb);
		}
		TimerTask tt = new TimerTask() {
			@Override
			public void run(Timeout timeout) {
				ContractResult cr = new ContractResult(ContractResult.Status.Error, "Timeout!");
				wakeUp(requestID, gson.toJson(cr));
			}
		};
		timer.newTimeout(tt, 10, TimeUnit.SECONDS);
	}

	public ContractResult syncSleep(String reuqestID) {
		final ContractResult cr = new ContractResult(ContractResult.Status.Error, "Timeout!!");
		ResultCallback cb = new ResultCallback() {
			@Override
			public void onResult(String str) {
				ContractResult ret = gson.fromJson(str, ContractResult.class);
				cr.status = ret.status;
				cr.result = ret.result;
				synchronized (this) {
					this.notifyAll();
				}
			}
		};
		sleep(reuqestID, cb);
		synchronized (cb) {
			try {
				cb.wait(20000L);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return cr;
	}

}