package com.yancloud.android.contractclient;

import java.util.List;


public class FunctionDesp {
	public FunctionDesp(String name, List<AnnotationNode> anno) {
		functionName = name;
		annotations = anno;
	}

	List<AnnotationNode> annotations;
	String functionName;
}