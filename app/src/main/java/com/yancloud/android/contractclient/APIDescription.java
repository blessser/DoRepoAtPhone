package com.yancloud.android.contractclient;

import com.google.gson.Gson;

public class APIDescription {
	public APIDescription(String name) {
		mName = name;
	}

	String mName;
	String desp;

	public void setDesp(String description) {
		desp = description;
	}

	public String toJson() {
		return new Gson().toJson(this);
	}

}