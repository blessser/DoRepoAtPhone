package com.yancloud.android.contractclient;

import java.util.ArrayList;
import java.util.List;

public class AnnotationNode {
	String type;
	List<String> args;

	public AnnotationNode(String type) {
		this.type = type;
		args = new ArrayList<>();
	}

	public void addArg(String arg) {
		args.add(arg);
	}

	public String getType() {
		return type;
	}

	public List<String> getArgs() {
		return args;
	}

}
