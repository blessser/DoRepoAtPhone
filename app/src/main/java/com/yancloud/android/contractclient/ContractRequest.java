package com.yancloud.android.contractclient;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import com.google.gson.Gson;

public class ContractRequest extends SM2Verifible implements Serializable {

	private static final long serialVersionUID = 5516141428163407726L;
	String contractID;
	// requester = reqPubKey
	String requester;
	// action = rsaPrivKeyEncoded(aesKey);
	String action;
	// arg = aesKeyEncoded({action,arg});
	String arg;
	String requestID;
	public boolean withDynamicAnalysis = false;

	public String getContractID() {
		return contractID;
	}

	public String getRequester() {
		return requester;
	}

	public String getArg() {
		return arg;
	}

	public String getAction() {
		return action;
	}

	public ContractRequest setContractID(String id) {
		contractID = id;
		return this;
	}

	public ContractRequest setAction(String action) {
		this.action = action;
		return this;
	}

	public ContractRequest setArg(String arg) {
		this.arg = arg;
		return this;
	}

	public static void main(String[] args) throws Exception {
		ContractRequest req = new ContractRequest();
		// req.requester = rsa.toBase64Pubkey();
		req.contractID = "123";
		req.action = "main";
		req.arg = "arg1";
		req.requester = "043d9f358b4de2d0ac3049f6d67719fc971611de92e44303b790598f948dae9eda30150b7855718f727049112509029a070cb2e7022a4d61ad7b03da227ebf78fb";
		req.signature = "8c54dd92dfe506af140b3e2e27899fe2dae73dbd022717fafcb52fd8f862edcf2d185739ad733c65ebfcfff6866b9b9c52b93537c92bbf822861048061887eef";
		System.out.println(new Gson().toJson(req));
		System.out.println(req.verifySignature());

	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

	@Override
	public String getPublicKey() {
		return requester;
	}

	@Override
	public String getContentStr() {
		return contractID + "|" + action + "|" + arg + "|" + requester;
	}

	@Override
	public void setPublicKey(String pubkey) {
		setRequester(pubkey);
	}

	public static ContractRequest parse(byte[] content) {
		try {
			ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(content));
			ContractRequest cr = (ContractRequest) input.readObject();
			return cr;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public byte[] toByte() {
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ObjectOutputStream bo = new ObjectOutputStream(out);
			bo.writeObject(this);
			return out.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void setRequestID(String str) {
		this.requestID = str;
	}

	public String getRequestID() {
		return requestID;
	}

}
