package com.yancloud.android.contractclient.databaseHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import java.util.ArrayList;
import java.util.List;


public class DatabaseDao {
    private DatabaseHelper databaseHelper;
    private SQLiteDatabase db;
    private static DatabaseDao dao;
    public DatabaseDao(Context context) {
        databaseHelper = DatabaseHelper.getInstance(context);
        db = databaseHelper.getReadableDatabase();
    }
    public static DatabaseDao getInstance(Context context){
        if(dao == null){
            dao = new DatabaseDao(context);
            Log.d("DOIP","Initalize Database");
        }
        return dao;
    }
    public static DatabaseDao getInstance(){
        if(dao == null){
            Log.e("DOIP","Database is null");
        }
        return dao;
    }

    public Do getInfoByDoi(String doi) {
        Cursor cursor = db.rawQuery("select * from doiTable where doi=?",
                new String[]{doi});
        Do doObject = new Do();
        if (cursor.moveToFirst()) {
            do {
                doObject.setDoi(cursor.getString(cursor.getColumnIndex("doi")));
                doObject.setPackageName(cursor.getString(cursor.getColumnIndex("package")));
                doObject.setMethodName(cursor.getString(cursor.getColumnIndex("method")));
                doObject.setCallNumber(cursor.getInt(cursor.getColumnIndex("callNumber")));
                doObject.setDesp(cursor.getString(cursor.getColumnIndex("desp")));
                doObject.setIsopen(cursor.getInt(cursor.getColumnIndex("isopen")));
//                doObject.setAppname(cursor.getString(cursor.getColumnIndex("appname")));

            } while (cursor.moveToNext());

        }
        return doObject;
    }
    public Do getInfoByPkgAndMethod(String packageName,String method){
        Cursor cursor=db.rawQuery("select * from doiTable where package=? and method =?",new String[]{packageName,method});
        Do doObject = new Do();
        if (cursor.moveToFirst()) {
            do {
                doObject.setDoi(cursor.getString(cursor.getColumnIndex("doi")));
                doObject.setPackageName(cursor.getString(cursor.getColumnIndex("package")));
                doObject.setMethodName(cursor.getString(cursor.getColumnIndex("method")));
                doObject.setCallNumber(cursor.getInt(cursor.getColumnIndex("callNumber")));
                doObject.setDesp(cursor.getString(cursor.getColumnIndex("desp")));
                doObject.setIsopen(cursor.getInt(cursor.getColumnIndex("isopen")));
//                doObject.setAppname(cursor.getString(cursor.getColumnIndex("appname")));
            } while (cursor.moveToNext());

        }
        return doObject;
    }
    public int isExist(String packageName,String method){
        Cursor cursor = db.rawQuery("select * from doiTable where package=? and method =?",
                new String[] { packageName,method });

        return cursor.getCount();
    }
    public void increCallNumberByDoi(String doi){
        ContentValues values = new ContentValues();
        Do doObject = getInfoByDoi(doi);
        int callNumber = doObject.getCallNumber();
        values.put("callNumber", callNumber+1);
        //参数依次是表名，修改后的值，where条件，以及约束，如果不指定三四两个参数，会更改所有行
        db.update("doiTable", values, "doi = ?", new String[]{doi});
    }
   public void change(String doi,int isopen)
   {
       ContentValues values = new ContentValues();
       Do doObject = getInfoByDoi(doi);
       values.put("isopen",isopen);
       db.update("doiTable", values, "doi = ?", new String[]{doi});
   }
    public List<Do> getAllDoi(){
        Cursor cursor = db.rawQuery("select * from doiTable", null);
        List<Do> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Do doObject = new Do();
                doObject.setDoi(cursor.getString(cursor.getColumnIndex("doi")));
                doObject.setPackageName(cursor.getString(cursor.getColumnIndex("package")));
                doObject.setMethodName(cursor.getString(cursor.getColumnIndex("method")));
                doObject.setCallNumber(cursor.getInt(cursor.getColumnIndex("callNumber")));
                doObject.setIsopen(cursor.getInt(cursor.getColumnIndex("isopen")));
                doObject.setDesp(cursor.getString(cursor.getColumnIndex("desp")));
                //doObject.setAppname(cursor.getString(cursor.getColumnIndex("appname")));
                list.add(doObject);
            } while (cursor.moveToNext());

        }
        return list;
    }
    public void insertDoi(String doi,String packageName,String methodName,Integer callNumber,int isopen,String desp){
        ContentValues value = new ContentValues();
        value.put("doi", doi)    ;
        value.put("package",packageName);
        value.put("method",methodName);
        value.put("callNumber",0);
        value.put("isopen",isopen);
        value.put("desp",desp);
        //value.put("appname",appname);
        db.insert("doiTable",null,value);    }

    /**
     * 查询所有数据
     *
     * @return Cursor
     */
   /* public Cursor findAll() {
        Cursor cursor = db.rawQuery("select * from t_stu", null);
        return cursor;
    }*/

    public void insert(String name, int age, String sex) {
       /* db.insert("insert into t_stu values(null,?,?,?)", new String[] { name,
                age + "", sex });*/
    }

    /**
     * 删除数据
     *
     * @param id
     */
    public void del(int id) {
        db.execSQL("delete from t_stu where _id=?", new String[] { id + "" });
    }

    /**
     * 根据id查询数据
     *
     * @param id
     * @return Cursor
     */
    public Cursor findById(int id) {
        Cursor cursor = db.rawQuery("select * from t_stu where _id=?",
                new String[] { id + "" });
        return cursor;
    }
    public String findByDoiTest(String id) {
       return "douying";
    }


    public void update(String name, int age, String sex, int id) {
        db.execSQL("update t_stu set s_name=?,s_age=?,s_sex=? where _id=?",
                new Object[] { name, age, sex, id });
    }

}
