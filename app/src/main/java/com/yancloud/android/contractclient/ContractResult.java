package com.yancloud.android.contractclient;

public class ContractResult {
	public ContractResult(Status status, String result) {
		this.status = status;
		this.result = result;
	}

	public enum Status {
		Success, Exception, Error
	}
	
	
	public Status status;
	public String result;
 }
