package com.yancloud.android.manager;

import android.os.Parcel;
import android.os.Parcelable;

public class PackageVersionPair implements Parcelable {
    public static final Creator<PackageVersionPair> CREATOR = new Creator<PackageVersionPair>() {
        @Override
        public PackageVersionPair createFromParcel(Parcel in) {
            return new PackageVersionPair(in);
        }

        @Override
        public PackageVersionPair[] newArray(int size) {
            return new PackageVersionPair[size];
        }
    };
    final public String pkgName;
    final public String versionName;

    public PackageVersionPair(String pkgName, String versionName) {
        this.pkgName = pkgName;
        this.versionName = versionName;
    }

    protected PackageVersionPair(Parcel in) {
        pkgName = in.readString();
        versionName = in.readString();
    }

    @Override
    public String toString() {
        return pkgName + "@" + versionName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pkgName);
        dest.writeString(versionName);
    }
}
